package br.com.agilb2b.model.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PrecoDto {

	@NotNull
	private Integer cor;
	@NotNull
	private Integer estampa;
	@NotNull
	private String tamanho;
	@NotNull
	private Integer produto;
	@NotNull
	private Integer tabela;
	@NotNull
	private BigDecimal preco;
	private BigDecimal precoPromocional;

}