package br.com.agilb2b.model.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TabelaVinculoGeradorDto {

	@NotNull
	private Integer idTabela;
	@NotNull
	private Integer idGerador; ////(Usu�rio, Cliente, Filial)
	@NotNull
	private String tipo; //FL (Filial) CL (Cliente) US (Usu�rio)
	
}