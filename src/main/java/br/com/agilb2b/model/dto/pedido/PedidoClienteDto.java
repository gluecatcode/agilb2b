package br.com.agilb2b.model.dto.pedido;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class PedidoClienteDto {

	private Integer pedido;
	
	private Integer cliente;
	
	private Integer endereco;
	
	private Integer vendedor;
	
	private Integer representante;
	
	private BigDecimal acrescimo;
	
	private BigDecimal desconto;
	
}