package br.com.agilb2b.model.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UsuarioDto {

	@NotNull
	private Integer id;
	@NotNull
	private String nome;
	@NotNull
	private String email;
	@NotNull
	private String tipo;
	@NotNull
	private Boolean ativo;
	
	private Integer vendedor;
	private Integer representante;
	
}