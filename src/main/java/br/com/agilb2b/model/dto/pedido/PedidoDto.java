package br.com.agilb2b.model.dto.pedido;
	
import java.math.BigDecimal;
import java.util.List;

import lombok.Data;
	
@Data
public class PedidoDto {
	
	private String codPedido;
	private String data;
	private String dataEntrega;
	
	private Integer tipo;
	private Boolean tipoAprovado;
	private Boolean tipoOrcamento;
	
	private Integer filial;
	private Integer tabela;
	private Integer condicaoPagamento;
	
	private Integer frete;
	private Integer freteTransportadora;
	private BigDecimal freteValor;
	private String freteDescr;
	private String freteCepDestino;
	private String freteNome;
	private String freteSobrenome;
	private Integer freteModalidade;
	
	private String file1;
	
	private BigDecimal cortesia;
	private String observacao;
	private String observacaoNota;
	private BigDecimal desconto;
	private BigDecimal acrescimo;
	
	private Integer quantidade;
	private BigDecimal valorTotal;
	private List<PedidoItemDto> itens;
	private List<PedidoClienteDto> clientes;
	
}