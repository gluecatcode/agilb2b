package br.com.agilb2b.model.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioVinculoGeradorDto {

	@NotNull
	private Integer idUsuario;
	@NotNull
	private Integer idCliente;
	
}