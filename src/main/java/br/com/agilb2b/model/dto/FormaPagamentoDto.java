package br.com.agilb2b.model.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FormaPagamentoDto {

	@NotNull
	private Integer id;
	@NotNull
	private String codigo;
	@NotNull
	private String descricao;
	@NotNull
	private Boolean ativo;
	
}