package br.com.agilb2b.model.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FilialDto {

	@NotNull
	private Integer id;
	private Integer idFornecedorErp;
	@NotNull
	private String codigo;
	@NotNull
	private String descricao;
	@NotNull
	private String cnpj;
	@NotNull
	private Boolean ativo;

}