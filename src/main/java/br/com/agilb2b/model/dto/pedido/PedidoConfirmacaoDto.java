package br.com.agilb2b.model.dto.pedido;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class PedidoConfirmacaoDto {
	
	@NotNull
	private Integer pedidob2b;
	@NotNull
	private Integer pedidoerp;
	
	private String errorMessage;
	
}