package br.com.agilb2b.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CorDto {

	private Integer id;
	private String codigo;
	private String descricao;
	private Boolean ativo;

}