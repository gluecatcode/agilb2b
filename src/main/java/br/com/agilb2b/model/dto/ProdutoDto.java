package br.com.agilb2b.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProdutoDto {

	@NotNull
	private Integer id;
	@NotNull
	private String referencia;
	@NotNull
	private String descricao;
	@NotNull
	private Date data;
	@NotNull
	private Boolean ativo;
	private String observacoes;
	private String observacoesSite;
	@NotNull
	private String busca;
	private Integer grade;
	private CategoriaDto categoria;
	private ColecaoDto colecao;
	private DepartamentoDto departamento;
	private DivisaoDto divisao;
	private GrupoDto grupo;
	private MarcaDto marca;
	private TipoDto tipo;
	private List<TamanhoDto> tamanhos;
	private List<EstampaDto> estampas;
	private List<CorDto> cores;
	private List<EspecificacaoDto> especificacoes;
	private List<ImagemDto> imagens;
	private BigDecimal peso;
	private BigDecimal largura;
	private BigDecimal altura;
	private BigDecimal comprimento;
	private Integer multiplo;

}
