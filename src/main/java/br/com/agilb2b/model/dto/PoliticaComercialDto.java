package br.com.agilb2b.model.dto;

import javax.validation.constraints.NotNull;

import br.com.agilb2b.model.type.TipoGerador;
import br.com.agilb2b.model.type.TipoPoliticaComercial;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PoliticaComercialDto {

	@NotNull
	private Integer formaPagamento;
	@NotNull
	private TipoPoliticaComercial tipo;
	@NotNull
	private TipoGerador tipoGerador;
	private Integer gerador;
	private Integer acerto;
	private Integer quantidade;
	
}
