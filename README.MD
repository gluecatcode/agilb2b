ÁgilB2B
========================

Plataforma Ecommerce B2B
------------------

Agilize seu departamento comercial através do ÁgilB2B. Este projeto público tem a finalidade de proporcionar
a integração com seu sistema ERP corrente. Escrito com a tecnologia Java, que permitirá desenvolver um projeto
de integração de forma desacoplada e escalável.
  
Entidades
------------------

As entidades estão contidas nos pacotes (br.com.agilb2b.model.dto e br.com.agilb2b.model.dto.pedido), 
declaradas com terminações "Dto".


Em breve mais informações...