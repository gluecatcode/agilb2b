package br.com.agilb2b.model.dto.pedido;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class PedidoItemDto {

	private Integer produto;
	private Integer cor;
	private Integer estampa;
	private String tamanho;
	private Integer quantidade;
	private BigDecimal preco;
	
	private BigDecimal acrescimo;
	private BigDecimal acrescimoPercent;
	private BigDecimal desconto;
	private BigDecimal descontoPercent;
	
	private BigDecimal alIpi;
	private BigDecimal baseIpi;
	private BigDecimal valorIpi;
	private BigDecimal alSub;
	private BigDecimal  baseSub;
	private BigDecimal valorSub;
	
	private String observacao;
	private Integer cliente;
	
}