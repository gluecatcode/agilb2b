package br.com.agilb2b.model.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EstoqueDto {

	@NotNull
	private Integer cor;
	@NotNull
	private Integer estampa;
	@NotNull
	private String tamanho;
	@NotNull
	private Integer produto;
	@NotNull
	private Integer filial;
	@NotNull
	private Integer quantidade;

}