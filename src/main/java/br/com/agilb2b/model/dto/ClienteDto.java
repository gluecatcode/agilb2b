package br.com.agilb2b.model.dto;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import br.com.agilb2b.model.type.TipoDocumento;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClienteDto {

	@NotNull
	private Integer id;
	@NotNull
	private Integer idEnderecoErp;
	private Integer idFilialErp;
	private Integer vendedor;
	private Integer representante;
	@NotNull
	private String codigo;
	@NotNull
	private String nome;
	@NotNull
	private String fantasia;
	@NotNull
	private String email;
	private Integer tipoEmpresa;
	private Date nascimento;
	@NotNull
	private String ddd;
	@NotNull
	private String telefone;
	@NotNull
	private TipoDocumento tipoDocumento;
	@NotNull
	private String numDocumento;
	private String numDocumento1;
	@NotNull
	private Boolean bloqueiaPedidos;
	@NotNull
	private String codigoIBGE;
	@NotNull
	private String cidade;
	@NotNull
	private String uf;
	@NotNull
	private Boolean ativo;
	
	private String endereco;
	private String numero;
	private String cep;
	private String bairro;
	private String complemento;
	private String contato;
	
	@Valid
	private List<EnderecoClienteDto> enderecos;
}