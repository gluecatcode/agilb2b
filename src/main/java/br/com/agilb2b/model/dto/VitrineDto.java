package br.com.agilb2b.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VitrineDto {

	private Integer id;
	private Integer indice;
	private Integer pai;
	private String descricao;
	private boolean excluido;
	private boolean visivel;
	
}
