package br.com.agilb2b.model.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PedidoClienteInfoDto {
	
	@NotNull
	private Integer idErp;
	@NotNull
	private String codigoErp;
	@NotNull
	private Integer status;
	@NotNull
	private String statusDescricao;
	private String nfs;
	private String numeroObjeto;
	private String chaveNfe;
	@NotNull
	private String data;

}