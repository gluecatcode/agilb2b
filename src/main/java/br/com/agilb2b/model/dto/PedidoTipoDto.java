package br.com.agilb2b.model.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PedidoTipoDto {

	@NotNull
	private Integer id;
	@NotNull
	private String tipo;
	@NotNull
	private String descricao;
	private Integer tipoFrete;
	@NotNull
	private Boolean aprovado;
	@NotNull
	private Boolean orcamento;
	@NotNull
	private Boolean validaEstoque;
	@NotNull
	private Boolean ativo;

}