package br.com.agilb2b.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EspecificacaoDto {

	private Integer id;
	private String nome;
	private String descricao;
	private Boolean caracteristica;
	private Boolean filtro;

}