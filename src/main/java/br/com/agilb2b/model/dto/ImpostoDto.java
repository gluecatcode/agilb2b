package br.com.agilb2b.model.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ImpostoDto {

	@NotNull
	private Integer produto;
	@NotNull
	private String estado;
	private BigDecimal ipi;
	private BigDecimal substIcms;
	private BigDecimal alIcms;
	private BigDecimal alIcmsOrigem;
	@NotNull
	private Boolean ativo;
	
}