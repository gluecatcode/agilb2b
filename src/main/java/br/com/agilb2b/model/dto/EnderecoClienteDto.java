package br.com.agilb2b.model.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EnderecoClienteDto {

	@NotNull
	private String uf;
	
	@NotNull
	private String endereco;
	
	@NotNull
	private String cidade;
	
	private String cep;
	
	@NotNull
	private String bairro;
	
	private String codigoIBGE;
	
	@NotNull
	private Integer idEnderecoErp;
	
	private String ddd;
	private String telefone;
	
	@NotNull
	private String numero;
	
	private String contato;
	
}