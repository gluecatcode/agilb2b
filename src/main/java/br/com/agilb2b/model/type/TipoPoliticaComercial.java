package br.com.agilb2b.model.type;

public enum TipoPoliticaComercial {

	DESCONTO,
	ACRESCIMO
}
