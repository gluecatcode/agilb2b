package br.com.agilb2b.model.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BoletoDto {

	@NotNull
	private Integer lancamento;
	@NotNull
	private Integer cliente;
	@NotNull
	private String numeroDocumento;
	@NotNull
	private String nossoNumero;
	@NotNull
	private String dataVencimento;
	@NotNull
	private BigDecimal valorBoleto;
	@NotNull
	private String sacadoNome;
	@NotNull
	private String sacadoCnpj;
	@NotNull
	private String sacadoCep;
	@NotNull
	private String sacadoBairro;
	@NotNull
	private String sacadoLogradouro;
	@NotNull
	private String sacadoCidade;
	@NotNull
	private String sacadoEstado;
	@NotNull
	private String codBanco;
	@NotNull
	private String conta;
	@NotNull
	private String agencia;
	@NotNull
	private String carteira;
	@NotNull
	private String dataEmissao;
	@NotNull
	private String cedenteNome;
	@NotNull
	private String cedenteCnpj;
	@NotNull
	private String instrucao1;
	@NotNull
	private String instrucao2;
	@NotNull
	private String instrucao3;
	@NotNull
	private Boolean ativo;
	
}