package br.com.agilb2b.model.dto;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TamanhoDto {

	@NotNull
	private Integer indice;
	@NotNull
	private String tamanho;

}
